#!/usr/bin/env python3

# Copyright © 2020 erzo <erzo@posteo.de>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the copying.txt file for more details.

"""
You can use this script to generate mail invitations or export
the user data in a different format.

It creates one file for each user based on a templatefile
containing a pattern and a csvfile containing the user data.
The pattern may contain wildcards in the str.format syntax.
https://docs.python.org/3/library/string.html#format-string-syntax
All columns of the csvfile are available as keyword arguments.
str.format is not passed any positional arguments.

example:
  $ cat >csvfile <<eof
  name, password
  x, 123
  y, 456
  z, 789
  eof
  $ cat > templatefile <<eof
  your username: {name}
  your password: {password}
  eof
  $ ./format-template.py csvfile templatefile
  $ ls -1 out
  x.txt
  y.txt
  z.txt
"""

import os
import csv
import gettext
_ = gettext.gettext

from main import Model, MyError

FILE_EXTENSION = ".txt"

def main(args):
	path_out = args.out
	if path_out is None:
		path_out = "out"
	check_path_out(path_out)

	with open(args.templatefile) as templatefile:
		template = templatefile.read()

	with open(args.csvfile, newline='') as csvfile:
		create_mails(csvfile, template, path_out)

def check_path_out(path):
	if os.path.isdir(path):
		ensure_empty(path)
	elif os.path.exists(path):
		raise MyError(_("{path_out} is a file").format(path_out=path))
	else:
		os.mkdir(path)

def ensure_empty(path):
	content = os.listdir(path)
	if not content:
		return

	content = [os.path.join(path, fn) for fn in content]
	n = len(FILE_EXTENSION)
	content = [fn for fn in content if os.path.isfile(fn) and fn[-n:] == FILE_EXTENSION]

	yes = _("y")
	no = _("n")

	if content:
		ans = input(_("{path_out} contains already {fileextension} files. Do you want to delete them? [y/N] ").format(path_out=path, fileextension=FILE_EXTENSION))
		ans = ans.strip()
		ans = ans.lower()
		if ans == yes:
			for fn in content:
				os.remove(fn)
			return

	else:
		ans = input(_("{path_out} is not empty but does not contain any {fileextension} files. Do you want to continue? [y/N] ").format(path_out=path, fileextension=FILE_EXTENSION))
		ans = ans.strip()
		ans = ans.lower()
		if ans == yes:
			return

	raise MyError(_("{path_out} is not empty.").format(path_out=path))


def create_mails(inputstream, template, path_out):
	csvreader = csv.reader(inputstream, skipinitialspace=True)
	headings = next(csvreader)
	users = [dict(zip(headings, cells)) for cells in csvreader]

	for u in users:
		out = template.format(**u)
		fn = u[Model.COL_NAME] + FILE_EXTENSION
		fn = os.path.join(path_out, fn)
		print(_("generating {filename}").format(filename=fn))
		with open(fn, mode="wt") as f:
			f.write(out)


if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument("csvfile", help=_("input file containing the user data"))
	parser.add_argument("templatefile", help=_("input file containing the pattern"))
	parser.add_argument("out", nargs=argparse.OPTIONAL, help=_("the output directory"))

	args = parser.parse_args()
	try:
		main(args)
	except MyError as excp:
		print(excp)
		exit(1)
