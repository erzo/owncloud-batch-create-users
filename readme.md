# main.py

	usage: main.py [-h] [-n] [csvfile]

	Batch create users for owncloud from a csv file.

	The first line is expected to be the column headers.
	The columns can have an arbitrary order.
	Recognized columns:
		- name
		- group
		- email
		- display name
		- password

	name is mandatory, the others optional.
	name is normalized, group and email are checked if they are valid.
	If password is omitted a random password is generated for each user.
	The data is printed to stdout so that the generated password
	can be saved. All other output is written to stderr.
	Unknown columns are ignored.

	If you are unhappy with the normalization of user names change
	the method Model.normalize_name.

	This program must be executed as the same user like the webserver.
	Please check if the following attributes are set correctly for you:
		OwncloudInterface.user
		OwncloudInterface.occ
	The attribute user is merely used for checking if this script is
	executed as the correct user.
	occ must contain the correct path to the owncloud-command-program.

	positional arguments:
	  csvfile        The input file providing the data which users to create.
			 Read from stdin if this is *not* given.

	optional arguments:
	  -h, --help     show this help message and exit
	  -n, --dry-run  print the commands to stderr instead of executing them and skip already existing users check

# preprocess.py

	usage: preprocess.py [-h] [-g GROUP] [file_in]

	Preprocess a csv file for batch creation of users for owncloud.

	This script generates the mandatory column name and the optional
	column display name based on first name, last name or email address.
	The result is printed to stdout.

	The first line is expected to be the column headers.
	The columns can have an arbitrary order.
	Recognized columns:
		- first name
		- last name
		- email

	You can customize the created name by changing
		Model.default_name_pattern
		Model.normalize_name

	positional arguments:
	  file_in

	optional arguments:
	  -h, --help            show this help message and exit
	  -g GROUP, --group GROUP
				add or overwrite a group column

# format-template.py

	usage: format-template.py [-h] csvfile templatefile [out]

	You can use this script to generate mail invitations or export
	the user data in a different format.

	It creates one file for each user based on a templatefile
	containing a pattern and a csvfile containing the user data.
	The pattern may contain wildcards in the str.format syntax.
	https://docs.python.org/3/library/string.html#format-string-syntax
	All columns of the csvfile are available as keyword arguments.
	str.format is not passed any positional arguments.

	example:
	  $ cat >csvfile <<eof
	  name, password
	  x, 123
	  y, 456
	  z, 789
	  eof
	  $ cat > templatefile <<eof
	  your username: {name}
	  your password: {password}
	  eof
	  $ ./format-template.py csvfile templatefile
	  $ ls -1 out
	  x.txt
	  y.txt
	  z.txt

	positional arguments:
	  csvfile       input file containing the user data
	  templatefile  input file containing the pattern
	  out           the output directory

	optional arguments:
	  -h, --help    show this help message and exit
