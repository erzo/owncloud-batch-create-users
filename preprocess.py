#!/usr/bin/env python3

# Copyright © 2020 erzo <erzo@posteo.de>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the copying.txt file for more details.

"""
Preprocess a csv file for batch creation of users for owncloud.

This script generates the mandatory column name and the optional
column display name based on first name, last name or email address.
The result is printed to stdout.

The first line is expected to be the column headers.
The columns can have an arbitrary order.
Recognized columns:
	- first name
	- last name
	- email

You can customize the created name by changing
	Model.default_name_pattern
	Model.normalize_name
"""

import csv
import re
import sys
import gettext
_ = gettext.gettext


class MyError(ValueError):
	pass


def main(args):
	if args.file_in is None:
		inputstream = sys.stdin
		preprocess(sys.stdin, args.group)
	else:
		filename = args.file_in
		with open(filename, newline='') as f:
			preprocess(f, args.group)

def preprocess(inputstream, group=None):
	csvreader = csv.reader(inputstream, skipinitialspace=True)
	headings = next(csvreader)
	model = Model(headings, csvreader, group=group)
	model.preprocess()
	model.print_csv()


class Model:

	# input columns
	COL_FIRST_NAME = "first name"
	COL_LAST_NAME = "last name"
	COL_EMAIL = "email"

	# output columns
	COL_NAME = "name"
	COL_DISPLAY_NAME = "display name"
	COL_GROUP = "group"

	known_headings = [COL_FIRST_NAME, COL_LAST_NAME, COL_EMAIL, COL_NAME, COL_DISPLAY_NAME, COL_GROUP]

	# settings
	name_pattern_first_last = "{first_name}-{last_name}"
	name_pattern_last_first = "{last_name}-{first_name}"
	name_pattern_first      = "{first_name}"
	name_pattern_last       = "{last_name}"
	name_pattern_mail       = "{mail}"

	# name_pattern_mail is a fallback pattern. Don't use it as default_name_pattern.
	default_name_pattern = name_pattern_first_last

	display_name_pattern_first_last = "{first_name} {last_name}"
	display_name_pattern_last_first = "{last_name}, {first_name}"
	display_name_pattern_first      = "{first_name}"
	display_name_pattern_last       = "{last_name}"
	display_name_pattern_mail       = "{mail}"

	# display_name_pattern_mail is a fallback pattern. Don't use it as display_default_name_pattern.
	display_default_name_pattern = display_name_pattern_first_last

	def __init__(self, headings, data, overwrite_name=False, group=None):
		self.overwrite_name = overwrite_name
		self.group = group
		self.given_headings = headings
		self.check_headings(headings)
		self.users = list()
		for cells in data:
			user = dict(zip(headings, cells))
			self.users.append(user)

	def check_headings(self, headings):
		for head in headings:
			if head not in self.known_headings:
				print("ignoring unkown column {colname!r}".format(colname=head), file=sys.stderr)


	def preprocess(self):
		if self.group and self.COL_GROUP not in self.given_headings:
			self.given_headings.append(self.COL_GROUP)
		if self.COL_DISPLAY_NAME not in self.given_headings:
			self.given_headings.append(self.COL_DISPLAY_NAME)
		if self.COL_NAME not in self.given_headings:
			self.given_headings.append(self.COL_NAME)

		for user in self.users:
			self.create_name(user)
			if self.group:
				user[self.COL_GROUP] = self.group

		self.assert_unique_names()

	def create_name(self, user):
		first_name = user.get(self.COL_FIRST_NAME, None)
		last_name = user.get(self.COL_LAST_NAME, None)
		mail = user.get(self.COL_EMAIL, None)

		if not first_name and not last_name:
			name_pattern = self.name_pattern_mail
			display_name_pattern = self.display_name_pattern_mail
		elif not first_name:
			name_pattern = self.name_pattern_last
			display_name_pattern = self.display_name_pattern_last
		elif not last_name:
			name_pattern = self.name_pattern_first
			display_name_pattern = self.display_name_pattern_first
		else:
			name_pattern = self.default_name_pattern
			display_name_pattern = self.display_default_name_pattern

		if name_pattern == self.name_pattern_mail:
			if not mail:
				raise MyError(_("no fallback info for user {user}").format(user=user))

			mail = mail.split('@')[0]
			display_mail = self.normalize_display_name(mail)
			display_name = display_name_pattern.format(mail=display_mail)

			mail = self.normalize_name(mail)
			name = name_pattern.format(mail=mail)

		elif name_pattern == self.name_pattern_first:
			display_first_name = self.normalize_display_name(first_name)
			display_name = display_name_pattern.format(first_name=display_first_name)

			first_name = self.normalize_name(first_name)
			name = name_pattern.format(first_name=first_name)

		elif name_pattern == self.name_pattern_last:
			display_last_name = self.normalize_display_name(last_name)
			display_name = display_name_pattern.format(last_name=display_last_name)

			last_name = self.normalize_name(last_name)
			name = name_pattern.format(last_name=last_name)

		else:
			display_first_name = self.normalize_display_name(first_name)
			display_last_name = self.normalize_display_name(last_name)
			display_name = display_name_pattern.format(first_name=display_first_name, last_name=display_last_name)

			first_name = self.normalize_name(first_name)
			last_name = self.normalize_name(last_name)
			name = name_pattern.format(first_name=first_name, last_name=last_name)

		if self.overwrite_name or self.COL_NAME not in user:
			user[self.COL_NAME] = name

		if self.overwrite_name or self.COL_DISPLAY_NAME not in user:
			user[self.COL_DISPLAY_NAME] = display_name

	def normalize_name(self, text):
		text = text.strip()
		text = text.replace(" ", "-")
		text = text.lower()
		text = text.replace("ä", "ae")
		text = text.replace("ö", "oe")
		text = text.replace("ü", "ue")
		text = text.replace("ß", "ss")

		return text

	def normalize_display_name(self, text):
		text = text.strip()
		text = text[:1].upper() + text[1:]

		return text

	def assert_unique_names(self):
		all_names = set()
		duplicate_names = list()
		for user in self.users:
			name = user[self.COL_NAME]
			if name in all_names:
				duplicate_names.append(name)
			else:
				all_names.add(name)

		if duplicate_names:
			duplicate_names = ", ".join(duplicate_names)
			raise MyError(_("duplicate user names: {names}").format(names=duplicate_names), file=sys.stderr)

	def print_csv(self):
		csvwriter = csv.writer(sys.stdout)
		csvwriter.writerow(self.given_headings)
		for user in self.users:
			row = [user[col] for col in self.given_headings]
			csvwriter.writerow(row)


if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument("file_in", nargs=argparse.OPTIONAL)
	parser.add_argument("-g", "--group", help=_("add or overwrite a group column"))

	args = parser.parse_args()

	try:
		main(args)
	except MyError as err:
		sys.stderr.write(str(err))
		sys.stderr.write('\n')
