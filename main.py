#!/usr/bin/env python3

# Copyright © 2020 erzo <erzo@posteo.de>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the copying.txt file for more details.

"""
Batch create users for owncloud from a csv file.

The first line is expected to be the column headers.
The columns can have an arbitrary order.
Recognized columns:
	- name
	- group
	- email
	- display name
	- password

name is mandatory, the others optional.
name is normalized, group and email are checked if they are valid.
If password is omitted a random password is generated for each user.
The data is printed to stdout so that the generated password
can be saved. All other output is written to stderr.
Unknown columns are ignored.

If you are unhappy with the normalization of user names change
the method Model.normalize_name.

This program must be executed as the same user like the webserver.
Please check if the following attributes are set correctly for you:
	OwncloudInterface.user
	OwncloudInterface.occ
The attribute user is merely used for checking if this script is
executed as the correct user.
occ must contain the correct path to the owncloud-command-program.
"""

import os
import pwd
import sys
import re
import csv
import json
import random
import subprocess
import gettext
_ = gettext.gettext


class MyError(ValueError):
	pass


def main(args):
	if args.csvfile is None:
		inputstream = sys.stdin
		create_users(sys.stdin, args.dry_run)
	else:
		filename = args.csvfile
		with open(filename, newline='') as f:
			create_users(f, args.dry_run)

def create_users(inputstream, dryrun):
	api = OwncloudInterface(test=dryrun)
	existing_users = api.get_existing_users()
	csvreader = csv.reader(inputstream, skipinitialspace=True)
	headings = next(csvreader)
	model = Model(headings, csvreader, existing_users)
	api.create_users(model)
	model.print_csv()


class Model:

	# see owncloud/occ user:add --help
	reo_valid_name = re.compile(r'^[A-Za-z0-9+_\-.]+$')
	reo_valid_email = re.compile(r'^[a-z0-9._\-]+@[a-z0-9_\-]+\.[a-z]+$', re.IGNORECASE)

	COL_NAME = "name"
	COL_PASSWORD = "password"
	COL_GROUP = "group"
	COL_EMAIL = "email"
	COL_DISPLAY_NAME = "display name"

	col_map = {
		COL_GROUP : '--group',
		COL_EMAIL : '--email',
		COL_DISPLAY_NAME : '--display-name',
	}

	known_headings = [COL_NAME, COL_PASSWORD] + list(col_map.keys())

	def __init__(self, headings, data, existing_users):
		self.users = []
		self.existing_users = existing_users
		self.given_headings = headings
		self.check_headings()
		for cells in data:
			user = dict(zip(headings, cells))
			user[self.COL_NAME] = self.normalize_name(user[self.COL_NAME])
			self.assert_valid_group_name(user)
			self.assert_valid_email(user)
			self.add_password(user)
			self.users.append(user)

		self.assert_unique_names()

	def check_headings(self):
		for head in self.given_headings:
			if head not in self.known_headings:
				print("ignoring unkown column {colname!r}".format(colname=head), file=sys.stderr)

		if self.COL_NAME not in self.given_headings:
			raise MyError(_("missing mandatory column {colname!r}".format(colname=self.COL_NAME)))

		if self.COL_PASSWORD not in self.given_headings:
			self.password_creator = PasswordCreator()
			self.given_headings.append(self.COL_PASSWORD)
		else:
			self.add_password = lambda x: None


	def assert_unique_names(self):
		all_names = set()
		duplicate_names = list()
		already_used_names = list()
		for user in self.users:
			name = user[self.COL_NAME]
			if name in self.existing_users:
				already_used_names.append(name)
			if name in all_names:
				duplicate_names.append(name)
			else:
				all_names.add(name)

		if duplicate_names or already_used_names:
			duplicate_names = ", ".join(duplicate_names)
			already_used_names = ", ".join(already_used_names)
			print(_("duplicate user names: {names}").format(names=duplicate_names), file=sys.stderr)
			print(_("already used user name: {names}").format(names=already_used_names), file=sys.stderr)
			raise MyError(_("there are duplicate or already existing names"))

	def add_password(self, user):
		password = self.password_creator.create_password()
		user[self.COL_PASSWORD] = password

	def normalize_name(self, text):
		text = text.replace(" ", "-")
		text = text.lower()
		text = text.replace("ä", "ae")
		text = text.replace("ö", "oe")
		text = text.replace("ü", "ue")
		text = text.replace("ß", "ss")

		if not self.reo_valid_name.match(text):
			raise MyError(_("invalid user name: {name}".format(name=text)))

		return text

	def assert_valid_email(self, user):
		email = user.get(self.COL_EMAIL, None)
		if email is None:
			return

		if self.reo_valid_email.match(email):
			return

		raise MyError(_("not a valid e-mail address: {email}".format(email=email)))

	def assert_valid_group_name(self, user):
		group = user.get(self.COL_GROUP, None)
		if group is None:
			return

		if self.reo_valid_name.match(group):
			return

		raise MyError(_("not a valid group name: {group}".format(group=group)))


	def get_users(self):
		return self.users

	def print_csv(self):
		csvwriter = csv.writer(sys.stdout)
		csvwriter.writerow(self.given_headings)
		for user in self.users:
			row = [user[col] for col in self.given_headings]
			csvwriter.writerow(row)


class PasswordCreator:

	chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' \
	        'abcdefghijklmonpqrstuvwxyz' \
	        '01234567890' \
	        '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'

	def __init__(self, length=8):
		# randint returns random number in a range including both end points [!]
		self.randint = random.randint
		self.max_i = len(self.chars) - 1
		self.length = length

	def create_password(self):
		return "".join(self.chars[self.randint(0,self.max_i)] for i in range(self.length))


class OwncloudInterface:

	occ = "/var/www/owncloud/occ"
	user = "www-data"

	def __init__(self, test=True):
		self.test = test
		if not self.test:
			self.assert_correct_user()
	
	def get_existing_users(self):
		if self.test:
			return []

		cmd = [self.occ, 'user:list', '--attributes', 'uid', '--output', 'json', '--no-interaction']
		p = subprocess.Popen(cmd, stdout=subprocess.PIPE, text=True)
		stdout, stderr = p.communicate()
		if p.returncode != 0:
			print("cmd: %s" % " ".join(cmd), file=sys.stderr)
			print(stdout, file=sys.stderr)
			raise MyError(_("failed to get existing users"))
		return json.loads(stdout).keys()

	def assert_correct_user(self):
		user = pwd.getpwuid(os.getuid()).pw_name
		if user != self.user:
			raise MyError(_("you must run this program as user {expected_user!r}, not as {is_user!r}".format(expected_user=self.user, is_user=user)))


	def create_users(self, model):
		for user in model.get_users():
			name = user[model.COL_NAME]
			password = user[model.COL_PASSWORD]
			cmd = [self.occ, "user:add", "--password-from-env", name]
			env = dict(OC_PASS=password)

			for col in model.col_map:
				if col in user:
					cmd.append(model.col_map[col])
					cmd.append(user[col])

			self.run(cmd, env)
	
	def run(self, cmd, env):
		if self.test:
			print("env: {env}".format(env=env), file=sys.stderr)
			print("cmd: {cmd}".format(cmd=cmd), file=sys.stderr)
		else:
			subprocess.run(cmd, env=env, check=True)


if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument("csvfile", nargs=argparse.OPTIONAL, help=_("The input file providing the data which users to create.\nRead from stdin if this is *not* given."))
	parser.add_argument("-n", "--dry-run", action="store_true", help=_("print the commands to stderr instead of executing them and skip already existing users check"))

	args = parser.parse_args()

	try:
		main(args)
	except MyError as err:
		sys.stderr.write(str(err))
		sys.stderr.write('\n')
